# Semestrální projekt pro SMP (Sběr a modelování požadavků) letní semestr 2023/2024 

## Stav - ve vývoji 🚧

## Docta. Aplikace pro procvičování probíraných látek předmětů na FEL

### 🔎 About

Projekt "Docta" je mobilní aplikace navržená pro studenty Fakulty Elektrotechniky (FEL), která umožňuje pohodlné procvičování a opakování probírané látky ze základních předmětů, jako je ZDM, ZWA, LAG, MAA, PRA a další.

Aplikace je přístupná prostřednictvím mobilního zařízení a umožňuje studentům studovat i během cestování tramvají nebo během přestávky.

Cílem projektu je usnadnit studentům procvičování učiva a zlepšit jejich znalosti a dovednosti v daných oblastech.

Pro pedagogy představuje "Docta" možnost zapojení interaktivních nástrojů do výuky a monitorování pokroku studentů, což může vést k vylepšení kvality výuky.

Projekt má ambici zvýšit efektivitu vzdělávání a zároveň zvýšit spokojenost studentů s výukou na FEL.

---

### :information_source: Wiki

Další informace o projektu a průběhu práce naleznete na [wiki](https://gitlab.fel.cvut.cz/yemtskse/smp-106-tym1-docta/-/wikis/home).

---

### 📞 Contacts

<details><summary>Klikněte pro rozšíření</summary>
&nbsp;

**Kseniia Yemtseva** - 📧 Email: [yemtskse@fel.cvut.cz](mailto:yemtskse@fel.cvut.cz)

**Dmytro Rastvorov** - 📧 Email: [rastvdmy@fel.cvut.cz](mailto:rastvdmy@fel.cvut.cz)

**Yehor Volodin** - 📧 Email: [volodyeh@fel.cvut.cz](mailto:volodyeh@fel.cvut.cz)

**Eleonora Virych** - 📧 Email: [virycele@fel.cvut.cz](mailto:virycele@fel.cvut.cz)

**Serhii Voronko** - 📧 Email: [voronse1@fel.cvut.cz](mailto:voronse1@fel.cvut.cz)

**Bilal Alian** - 📧 Email: [alianbil@fel.cvut.cz](mailto:alianbil@fel.cvut.cz)

</details>
